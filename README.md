### Dog Breed Classification###

# Summary #
The objective of this project is to perform classification on [Stanford Dogs Dataset](http://vision.stanford.edu/aditya86/ImageNetDogs/). The dataset contains 20,580 images of 120 breeds of dogs from around the world. The [Keras](http://keras.io/) deep learning library has been used to perform the model training. The 16-layer neural network used by the VGG team in the ILSVRC-2014 competition will be used as the initial model. I have followed the recipe explained [here](https://www.kaggle.com/c/state-farm-distracted-driver-detection/forums/t/20747/simple-lb-0-23800-solution-keras-vgg-16-pretrained). 

# Prerequisites #
* Technical requirements: The code is written in Python3 (3.5.1+) and uses Numpy (1.10.4), Theano (0.8.2), Keras (1.0.0) and h5py (2.6.0) libraries. To install these libraries, run the following command:

```
sudo pip3 install -r requirements.txt
```

* Pre-trained model: Download the pre-trained mode from [here](https://drive.google.com/file/d/0Bz7KyqmuGsilT0J5dmRCM0ROVHc/view?usp=sharing) and put it inside *model* directory.

Once all the requirements are met, training neural network can be performed by executing by the following command:

```
./train.sh cpu
```
or
```
./train.sh gpu#
```
depending on which processing unit the code is going to run. # should be substituted with a gpu number.
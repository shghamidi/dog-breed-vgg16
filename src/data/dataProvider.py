import numpy as np
import os
from PIL import Image
import h5py

class dataProvider:
    def __init__(self, config, splitedBreedsList, dataType):
        self.dataType = dataType
        self.totalDataNum = sum([len(splitedBreedsList[i][dataType.value]) for i in range(len(splitedBreedsList))])
        self.preProcessData(config, splitedBreedsList)
           

    def preProcessData(self,config, splitedBreedsList):
        if not os.path.isfile(os.path.join(config.dataDirPath, self.dataType.name+".hdf5")):       
            h5f = h5py.File(os.path.join(config.dataDirPath, self.dataType.name+".hdf5"), "w")
            dsetFeature = h5f.create_dataset('feature', (self.totalDataNum, 3, config.imgRows, config.imgCols), dtype=np.int32)
            dsetLabel = h5f.create_dataset('label', (self.totalDataNum, ), dtype=np.int32)
            
            # To randomize data while saving        
            idxList = np.arange(self.totalDataNum, dtype="int32")
            np.random.shuffle(idxList)
            
            cnt = 0
            for index, dogList in enumerate(splitedBreedsList):                    
                for dogs in dogList[self.dataType.value]:                
                    img = Image.open(os.path.join(config.dataDirPath, "Images", dogList[0], dogs))
                    img = img.resize((config.imgRows, config.imgCols), Image.ANTIALIAS)            
                    imgArray = np.asarray(img)
                    imgArray = np.transpose(imgArray, axes = [2,0,1])                     
                     
                    dsetFeature[idxList[cnt]] = imgArray[0:3]
                    dsetLabel[idxList[cnt]] = index        
                    cnt += 1
            h5f.close()
        else:
            print("File %s already exists. Not performing preprocessing." %(os.path.join(config.dataDirPath, self.dataType.name+".hdf5")))
            
 
    def batchGenerator(self, config):
        
        # For image augmentation
        meanPixel = [103.939, 116.779, 123.68]
       
        while(1):
            h5f = h5py.File(os.path.join(config.dataDirPath, self.dataType.name+".hdf5"), "r")
            batchNum = int(np.ceil(self.totalDataNum/float(config.batchSize)))
            
            for i in range(batchNum):
                feature = h5f['feature'][i*config.batchSize: (i+1)*config.batchSize]
                feature = feature.astype(np.float32)
                label = h5f['label'][i*config.batchSize: (i+1)*config.batchSize]
                label = np.eye(config.classNum, config.classNum)[label]
                
                for c in range(3):
                    feature[:, c, :, :] -= meanPixel[c]
                    
                               
                # shuffle!
                idxList = np.arange(feature.shape[0], dtype="int32")
                np.random.shuffle(idxList)                
                feature = feature[idxList]
                label = label[idxList]
                                
                yield  feature, label
                                
            h5f.close()

import os
from enum import Enum

from keras.callbacks import ModelCheckpoint, Callback

from classify.neuralNetwork import neuralNetwork
from data.prepareData import splitData
from data.dataProvider import dataProvider

class dataType(Enum):
    training = 1
    validation = 2
    test = 3

class config(object):
    # NN structure config
    imgRows, imgCols = 224, 224
    batchSize = 128
    classNum = 120
    
    # NN training config
    learningRate = .01    
    learningRateDecay = 1e-6
    momentum = 0.9
    epochNum = 100
     
    # Data configs
    trainingPercentage = 0.6
    valPercentage = 0.2
    dataDirPath = "../../dataset/"
    dataDirPath = "./dataset/"
    modelToSave = "../../model/"
    modelToSave = "./model/"

# Evaluate at the end of each epoch
class EvaluateHistory(Callback):
    def __init__(self, model, dataGen, totalDataNum):
        super(Callback, self).__init__()
        self.model = model
        self.dataGen = dataGen
        self.totalDataNum = totalDataNum        
        
    def on_epoch_end(self, epoch, logs={}):        
        score = self.model.evaluate_generator(self.dataGen, val_samples=self.totalDataNum)
        print("\n"+"eval_loss: %0.4f - eval_acc: %0.4f" %(score[0], score[1])) 


def main():
        
    print("Preparing data. This might take some time ...")
    splitedBreedsList = splitData(config.dataDirPath, config.trainingPercentage, config.valPercentage)
    
    trainingDataProvider = dataProvider(config, splitedBreedsList, dataType.training)
    valDataProvider = dataProvider(config, splitedBreedsList, dataType.validation)
    testDataProvider = dataProvider(config, splitedBreedsList, dataType.test)
    
    trainingDataGenerator = trainingDataProvider.batchGenerator(config)    
    valDataGenerator = valDataProvider.batchGenerator(config)
    testDataGenerator = testDataProvider.batchGenerator(config)

    print("Building network ...")
    net = neuralNetwork(config)
    net.model.summary()
    
    # Save the model weights after each epoch
    os.makedirs(os.path.join(config.modelToSave), exist_ok=True)
    filepath=os.path.join(config.modelToSave, "weights.{epoch:02d}.hdf5")
    checkpointer = ModelCheckpoint(filepath=filepath, monitor='val_loss', verbose=0, save_best_only=False, mode='auto')

    evaluator = EvaluateHistory(net.model, testDataGenerator, testDataProvider.totalDataNum)
        
    print("Training network ...")    
    net.model.fit_generator(trainingDataGenerator, samples_per_epoch=trainingDataProvider.totalDataNum, 
                            validation_data=valDataGenerator, nb_val_samples=valDataProvider.totalDataNum, 
                            nb_epoch=config.epochNum, callbacks=[checkpointer, evaluator])
    
  
if __name__ == '__main__':
    main()

#!/bin/bash

# Usage: ./train.sh cpu or ./train.sh gpu0 

cpu=$1 
currentPath=`pwd`

THEANO_FLAGS="device=${cpu}" PYTHONPATH=$PYTHONPATH:${currentPath}/src python3 -u ${currentPath}/src/example/trainNN.py < /dev/null > ${currentPath}/train.log 2>&1 

